package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class GetOrderDetail(
    @field:SerializedName("OrderIdProduct")
    val orderIdProduct: List<OrderIdProductItem?>? = null
)

data class OrderIdProductItem(
    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("batch_no")
    val batchNo: String? = null,

    @field:SerializedName("qty")
    val qty: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("bay")
    val bay: String? = null,

    @field:SerializedName("sku")
    val sku: String? = null,

    @field:SerializedName("order_id")
    val orderId: Int? = null,

    @field:SerializedName("permit_no")
    val permitNo: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)
