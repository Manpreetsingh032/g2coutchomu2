package `in`.his.g2coutchomu2.dataModel

import com.google.gson.annotations.SerializedName

data class BayResponse(

    @field:SerializedName("bay")
    val bay: List<BayItem?>? = null
)

data class BayItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("bay")
    val bay: String? = null,

    @field:SerializedName("capacity")
    val capacity: Double? = null
)
