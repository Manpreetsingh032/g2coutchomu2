package `in`.his.g2coutchomu2.dataModel

class InsertOutGoods(
    var batch_no: String? = null,
    var bay_no: String? = null,
    var sku: String? = null,
    var qty: String? = null,
    var order_id: String? = null
) {

    /*
    * {"batch_no":"1","bay_no":"A1","sku":"BPWQ011CHDCVL","qty":"10"}*/
}