package `in`.his.g2coutchomu2.dataModel

import com.google.gson.annotations.SerializedName

data class LocationChangeResponse(

    @field:SerializedName("message")
    val message: String? = null
)
