package `in`.his.g2coutchomu2.dataModel

import com.google.gson.annotations.SerializedName

data class LocationChangeData(

    @field:SerializedName("LocationChange")
    val locationChange: List<LocationChangeItem?>? = null
)

data class LocationChangeItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("batch_no")
    val batchNo: String? = null,

    @field:SerializedName("qty")
    val qty: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("bay_no")
    val bayNo: String? = null,

    @field:SerializedName("sku")
    val sku: String? = null,

    @field:SerializedName("old_bay")
    val oldBay: String? = null,

    @field:SerializedName("location_status")
    val locationStatus: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)
