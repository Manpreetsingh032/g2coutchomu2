package `in`.his.g2coutchomu2.dataModel

import com.google.gson.annotations.SerializedName

data class TransportDetails(

    @field:SerializedName("TransportStatusData")
    val transportStatusData: List<TransportStatusDataItems?>? = null
)

data class TransportStatusDataItems(

    @field:SerializedName("driver_name")
    val driverName: String? = null,

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("truck_bay_no")
    val truckBayNo: Any? = null,

    @field:SerializedName("address")
    val address: String? = null,

    @field:SerializedName("party_name")
    val partyName: String? = null,

    @field:SerializedName("contact_no")
    val contactNo: String? = null,

    @field:SerializedName("vehicle_no")
    val vehicleNo: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("state")
    val state: String? = null,

    @field:SerializedName("permit_no")
    val permitNo: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)
