package `in`.his.g2coutuk

import `in`.his.g2coutchomu2.Utils
import `in`.his.g2coutchomu2.dataModel.ProductionItem

class Search {
    fun searchFromBay(bayName: String?): ArrayList<ProductionItem> {
        val searchList = ArrayList<ProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.bayNo.equals(bayName, true)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromAllExceptEmpty(empty: String?): ArrayList<ProductionItem> {
        val searchList = ArrayList<ProductionItem>()
        try {
            for (store in Utils.allData) {
                if (!store!!.sku.equals(empty, true)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromAll(keySearch: String): ArrayList<ProductionItem> {
        val searchList = ArrayList<ProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.sku!!.contains(keySearch, true)
                    || store.batchNo!!.toString().contains(keySearch, true)
                    || store.bayNo!!.contains(keySearch, true)
                ) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromBlock(block: String): ArrayList<ProductionItem> {
        val searchList = ArrayList<ProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.bayNo!!.contains("" + block, true)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromBatch(batchNo: String): ArrayList<ProductionItem> {
        val searchList = ArrayList<ProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.batchNo.toString().contains(batchNo)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun getSkuItem(sku: String?): ArrayList<ProductionItem> {
        val searchList = ArrayList<ProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.sku.toString().contentEquals(sku!!)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }
}