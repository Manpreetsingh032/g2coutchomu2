package `in`.his.g2coutchomu2

import `in`.his.g2coutuk.dataModel.OrderIdProductItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_bay_values.view.*
import java.util.*

class FirstOrderListAdapter : RecyclerView.Adapter<FirstOrderListAdapter.MyViewHolder>() {
    private var list: ArrayList<OrderIdProductItem?> = ArrayList()
    private lateinit var setItemClick: SetFirstItemClick
    private var holdStatus = true
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        //if(!list.get(position).getSku().equalsIgnoreCase("empty")){
        holder.itemView.item_number_count.text = (position + 1).toString()
        holder.itemView.item_data_of_bay.text = list[position]?.bay
        holder.itemView.item_data_of_sku.text = list[position]?.sku
        holder.itemView.item_data_of_batch_no.text = list[position]?.batchNo.toString()
        holder.itemView.item_data_of_qty.text = list[position]?.qty.toString()

        holder.itemView.setOnClickListener {
            if (!holdStatus)
                setItemClick.onClick(
                    list[position]?.bay,
                    list[position]?.sku,
                    list[position]?.batchNo,
                    list[position]?.qty,
                    list[position]?.orderId
                )
        }

        if (holder.itemView.item_data_of_qty.text.toString() == "0") {
            holder.itemView.setBackgroundResource(R.color.green)
        } else {
            holder.itemView.setBackgroundResource(R.color.red)
        }

        if (holdStatus) {
            holder.itemView.setBackgroundResource(R.color.yellow)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bay_values, parent, false)
        return MyViewHolder(v)
    }

    fun setList(
        list: ArrayList<OrderIdProductItem?>,
        firstHoldStatus: Boolean
    ) {
        this.list = list
        holdStatus = firstHoldStatus
        notifyDataSetChanged()
    }

    fun addItemClick(setItemClick: SetFirstItemClick) {
        this.setItemClick = setItemClick
    }

    /**
     * View holder class
     */
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    companion object {
        private var instance: FirstOrderListAdapter? = null
        fun getInstance(): FirstOrderListAdapter {
            return if (instance == null) {
                instance = FirstOrderListAdapter()
                instance!!
            } else {
                instance!!
            }
        }
    }
}

interface SetFirstItemClick {
    fun onClick(
        bay: String?,
        sku: String?,
        batchNo: String?,
        qty: Int?,
        orderId: Int?
    )
}

/*extends
        RecyclerView.Adapter<firstOrderListAdapter.MyViewHolder> private List<order_details.OrderDetailsBean> list;
    Context context;
    private boolean enableselection;
    private static SparseBooleanArray itemStateArray = new SparseBooleanArray();

    public static SparseBooleanArray getCheckedItems() {
        return itemStateArray;
    }




    public firstOrderListAdapter(Context context, List<order_details.OrderDetailsBean> orderDetailsBeanArrayList
    ) {

        this.list = orderDetailsBeanArrayList;
        this.context = context;

        Log.d("TAG", "SSConstructor");
    }


    */
/**
 * View holder class
 */
/*
public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView bayNo, batchNo, sku, qty, Number;
    LinearLayout BackLay;
    public MyViewHolder(View view) {
        super(view);
        bayNo = view.findViewById(R.id.data_of_bay);
        sku = view.findViewById(R.id.data_of_sku);
        batchNo = view.findViewById(R.id.data_of_batch_no);
        qty = view.findViewById(R.id.data_of_qty);
        Number = view.findViewById(R.id.number_count);
        BackLay = view.findViewById(R.id.ll_show_value_single_bay);
        Number.setSelected(true);
        bayNo.setSelected(true);
        sku.setSelected(true);
        batchNo.setSelected(true);
        qty.setSelected(true);
        first_order.setEnabled(false);
        order_complete_first.setEnabled(false);
        view.setOnClickListener(this);
    }

    void bind(int i) {
        if (!itemStateArray.get(i, false)) {
            BackLay.setBackgroundResource(R.color.colorC);
        } else {
            BackLay.setBackgroundResource(R.color.colorC);
        }
    }


    @Override
    public void onClick(View v) {
        int adapterPosition = getAdapterPosition();
        if (list.get(adapterPosition).getFlag() == 0) {
            String no = "";
            String bay = String.valueOf(list.get(adapterPosition).getBay_no());
            String sku = String.valueOf(list.get(adapterPosition).getSku());
            String lot = String.valueOf(list.get(adapterPosition).getLot_no());
            int tempQty = list.get(adapterPosition).getQuantity();
            List<Store> stores = MainActivity.myAppDatabase.myDao().getData();
            for (Store store : stores) {
                if (store.getBayno().equalsIgnoreCase(bay) && store.getSku().equalsIgnoreCase(sku)) {
                    no = String.valueOf(store.getSrNo());
                }
            }
            dialogModifyEntry.show();
            order1++;
            getDataFromRvBay(no, bay, sku, lot, tempQty);
            list.get(adapterPosition).setFlag(1);
            BackLay.setBackgroundResource(R.color.colorC);
            Log.d("TAG", bay+sku+lot+tempQty);
            if (order1 == list.size()) {
                order_complete_first.setEnabled(true);
                first_order.setEnabled(true);
            }
        }
    }
}


@Override
public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
    holder.Number.setText(String.valueOf(position + 1));
    holder.bayNo.setText(list.get(position).getBay_no());
    holder.sku.setText(list.get(position).getSku());
    holder.batchNo.setText(String.valueOf(list.get(position).getLot_no()));
    holder.qty.setText(String.valueOf(list.get(position).getQuantity()));
    if (list.get(position).getFlag() == 1) {
        holder.bind(position);
    }
}

@Override
public int getItemCount() {
    return list.size();
}

@NonNull
@Override
public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_bay_values, parent, false);
    return new MyViewHolder(v);
}
*/
