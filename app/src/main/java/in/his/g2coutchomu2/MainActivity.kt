package `in`.his.g2coutchomu2

import `in`.his.g2coutchomu2.*
import `in`.his.g2coutchomu2.dataModel.InsertModel
import `in`.his.g2coutchomu2.dataModel.InsertOutGoods
import `in`.his.g2coutchomu2.dataModel.LocationChangeItem
import `in`.his.g2coutchomu2.dataModel.ProductionItem
import `in`.his.g2coutuk.*
import `in`.his.g2coutuk.dataModel.*
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.OnInitListener
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.change_loc_msg.*
import kotlinx.android.synthetic.main.chomu.*
import kotlinx.android.synthetic.main.first_order.*
import kotlinx.android.synthetic.main.process_order_values_dialog.*
import kotlinx.android.synthetic.main.search.*
import kotlinx.android.synthetic.main.second_order.*
import kotlinx.android.synthetic.main.spinner_dialog_block.*
import kotlinx.android.synthetic.main.spinner_dialog_sku.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), OnInitListener {
    private lateinit var skuDialog: Dialog
    private lateinit var blockDialog: Dialog
    private lateinit var orderProcessDialog: Dialog
    private lateinit var changeProcessDialog: Dialog
    var searchSpinnerItem = ""
    var keySearch = ""
    lateinit var searchAdapter: SearchAdapter
    private lateinit var firstOrderListAdapter: FirstOrderListAdapter
    private lateinit var secondOrderListAdapter: SecondOrderListAdapter
    lateinit var firstChangeOrderAdapter: ChangeOrderAdapter
    lateinit var secondChangeOrderAdapter: ChangeOrderAdapter
    private var tts1: TextToSpeech? = null
    var oldProcessQty = 0
    var newProcessQty = 0
    private var currentOrderId = 0
    private var changeInQty = 0
    private var firstList = ArrayList<OrderIdProductItem?>()
    private var secondList = ArrayList<OrderIdProductItem?>()

    //    private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    private var mAudioManager: AudioManager? = null

    private lateinit var handleSharedPreference: HandleSharedPreference

    private lateinit var utils: Utils

    private lateinit var mqtt: MQtt
    lateinit var viewPager: ViewPager
    lateinit var clickObjHandle: ClickObjHandle
    private lateinit var handleClick: HandleClick


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mqtt = MQtt.managerInstance

        mqtt.mQTTConnect(this, resources.getString(R.string.app_name))

        mqttHandle()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        clickObjHandle = ClickObjHandle.instance()
        clickObjHandle.addClick(object : HandleClick {
            override fun clickBay(bay: String) {
                val list = Search().searchFromBay(bay)
                searchAdapter.setList(list)
                tts(list)
            }
        })
        ViewListener.instance().addBayView(object : SetBayView {
            override fun changeView() {
                findEmptyBay()
            }
        })

        this.handleClick = ClickObjHandle.instance().handleClick
/*
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        viewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tab_layout)
        tabs.setupWithViewPager(viewPager)

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))

        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                ViewListener.instance().changeView()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                ViewListener.instance().changeView()
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                ViewListener.instance().changeView()
            }

        })*/

        //Initialize TTS
        tts1 = TextToSpeech(this, this)
        tts1!!.language = Locale.ENGLISH
        tts1!!.setSpeechRate(.80.toFloat())

        utils = Utils()

        dataResponse()

        mAudioManager = getSystemService(AUDIO_SERVICE) as AudioManager

        try {
            setDialogs()
        } catch (e: Exception) {
        }
        try {
            buttonClicks()
        } catch (e: Exception) {
        }
        try {
            setSearchAdapter()
        } catch (e: Exception) {
        }

        handleSharedPreference = HandleSharedPreference(this)

        firstOrderId = handleSharedPreference.firstOrderNoPref()!!
        secondOrderId = handleSharedPreference.secondOrderNoPref()!!

        pullToRefresh.setOnRefreshListener {
            pullToRefresh.isRefreshing = false
            try {
                getAllDataCall()
                utils.getCommonList()
                ViewListener.instance().changeView()
            } catch (e: Exception) {
            }
        }
        try {
            getAllDataCall()
        } catch (e: Exception) {
        }
    }

    fun getAllDataCall() {
        utils.getAllData()
//        findEmptyBay()
    }

    private fun mqttHandle() {
        try {
            mqtt.addConnectionCalls(object : ConfirmConnection {
                override fun onSuccess() {
                    mqtt.subscribeTopic(this@MainActivity, "location")
                    mqtt.subscribeTopic(this@MainActivity, "status")
                }

                override fun onUnSuccess() {
                    try {
                        Handler(Looper.getMainLooper()).postDelayed({
                            mqtt.mQTTConnect(
                                this@MainActivity,
                                resources.getString(R.string.app_name)
                            )
                        }, 30000)
                    } catch (e: Exception) {
                        mqtt.mQTTConnect(
                            this@MainActivity,
                            resources.getString(R.string.app_name)
                        )
                    }
                }
            })
        } catch (e: Exception) {
        }

        try {
            mqtt.setValueListener(object : MqttValues {
                override fun onStatus(status: String) {
                    if (firstOrderId == status) {
                        setFirstOrder()
                    } else if (secondOrderId == status) {
                        setSecondOrder()
                    }
                }

                override fun onLocation(location: String) {
                    /*mqtt.publish("location", "DataChange".toByteArray())*/
                    if (location == "DataChange") {
                        getAllDataCall()
                        ViewListener.instance().changeView()
                    }
                }

                override fun onOthers(topic: String, string: String) {

                }
            })
        } catch (e: Exception) {
        }
    }

    private fun findEmptyBay() {

        setBack("B-8", b8)
        setBack("B-7", b7)
        setBack("B-6", b6)
        setBack("B-5", b5)
        setBack("B-4", b4)
        setBack("B-3", b3)
        setBack("B-2", b2)
        setBack("B-1", b1)

    }
    private fun setBack(name: String, layout: TextView) {
        try {
            val list = Search().searchFromBay(name)
            if (list[0].sku.equals("empty", true)) {
                layout?.background =
                    ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
            } else {
                layout?.background =
                    ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
            }
        } catch (e: Exception) {
            Log.d("TAG", "setBack: $e")
        }
    }

    private fun setDialogs() {
        changeProcessDialog = Dialog(this)
        with(changeProcessDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.change_loc_msg)
            setCanceledOnTouchOutside(true)
            setCancelable(true)
        }

        orderProcessDialog = Dialog(this)
        with(orderProcessDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.process_order_values_dialog)
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(false)
            setCancelable(false)

            cancel_order.setOnClickListener {
                dismiss()
                order_batch_no?.text = ""
                order_bay_no?.text = ""
                order_qty?.setText("")
                order_sku?.text = ""
                total_order_qty?.text = resources.getString(R.string.total_order_qty)
            }

            submit_order.setOnClickListener {
                try {
                    if (order_qty.text.isNullOrBlank()) {
                        Toast.makeText(
                            this@MainActivity,
                            "Please enter some quantity",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        submitSingleItem(
                            currentOrderId,
                            order_bay_no?.text.toString(),
                            order_batch_no?.text.toString(),
                            order_sku?.text.toString(),
                            newProcessQty
                        )
                    }
                } catch (e: Exception) {
                }
            }

            order_qty?.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(p0: Editable?) {

                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                @SuppressLint("SetTextI18n")
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (p0?.isNotEmpty()!!) {
                        try {
                            changeInQty = order_qty.text.toString().toInt()
                            newProcessQty = oldProcessQty - changeInQty
                        } catch (e: Exception) {
                        }

                        try {
                            if (newProcessQty >= 0) {
                                total_order_qty.text =
                                    resources.getString(R.string.total_order_qty) + " $newProcessQty "
                            } else {
                                Toast.makeText(
                                    this@MainActivity,
                                    "Value should not exceed total order value",
                                    Toast.LENGTH_LONG
                                ).show()
                                order_qty.setText("")
                            }
                        } catch (e: Exception) {
                        }
                    } else {
                        total_order_qty.text =
                            resources.getString(R.string.total_order_qty) + " $oldProcessQty "
                    }
                }
            })
        }

        skuDialog = Dialog(this)
        with(skuDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.spinner_dialog_sku)
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(true)
            setCancelable(true)

            try {
                val adapter = ArrayAdapter(
                    this@MainActivity,
                    android.R.layout.simple_list_item_1, android.R.id.text1, Utils.skusList
                )
                list_sku.adapter = adapter

                list_sku.onItemClickListener =
                    OnItemClickListener { _: AdapterView<*>?, _: View?, position: Int, _: Long ->
                        val skuItem = adapter.getItem(position)
                        Log.d("TAG", "init: $skuItem")
                        skuDialog.dismiss()
                        val list = Search().getSkuItem(skuItem)
                        searchAdapter.setList(list)
                        tts(list)
                        tv_search?.text = skuItem
                    }
            } catch (e: Exception) {
            }
        }

        blockDialog = Dialog(this)
        with(blockDialog) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.spinner_dialog_block)
            window!!.attributes.windowAnimations = R.style.DialogTheme
            setCanceledOnTouchOutside(true)
            setCancelable(true)

            block_a.setOnClickListener {
                try {
                    val list = Search().searchFromBlock("A")
                    searchAdapter.setList(list)
                    tts(list)
                    tv_search?.setText(R.string.block_a)
                    dismiss()
                } catch (e: Exception) {
                }
            }
            block_B?.setOnClickListener {
                try {
                    val list = Search().searchFromBlock("B")
                    searchAdapter.setList(list)
                    tts(list)
                    tv_search?.setText(R.string.block__b)
                    dismiss()
                } catch (e: Exception) {
                }
            }
            block_C?.setOnClickListener {
                try {
                    val list = Search().searchFromBlock("C")
                    searchAdapter.setList(list)
                    tts(list)
                    tv_search?.setText(R.string.block__c)
                    dismiss()
                } catch (e: Exception) {
                }
            }
        }

        try {
            val searchAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.search_spinner, R.layout.support_simple_spinner_dropdown_item
            )
            searchAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            search_spinner?.adapter = searchAdapter
        } catch (e: Exception) {
        }
    }

    @SuppressLint("SetTextI18n")
    private fun buttonClicks() {
        searchClicks()

        order_search.setOnClickListener {
            if (wms_search.isVisible) {
                showOrderScreen()
                order_search.text = resources.getString(R.string.search)
            } else {
                order_search.text = resources.getString(R.string.order)
                setSearch()
            }
        }

        first_get_order.setOnClickListener {
            setFirstOrder()
        }
        second_get_order.setOnClickListener {
            setSecondOrder()
        }

        second_order_complete?.setOnClickListener {
            if (secondId.isNotEmpty() && secondOrderComplete) {
                utils.updateOrderStatus(secondId, "2",this)
                second_order_complete?.isEnabled = false
                second_get_order?.isEnabled = true
                secondOrderComplete = false
            } else if (secondHoldStatus) {
                utils.updateOrderStatus(secondId, "4",this)
                second_order_complete?.isEnabled = false
                second_get_order?.isEnabled = true
                secondOrderComplete = false
                updateProduction(secondList)
            }
        }

        first_order_complete?.setOnClickListener {
            if (firstId.isNotEmpty() && firstOrderComplete) {
                utils.updateOrderStatus(firstId, "2",this)
                first_order_complete?.isEnabled = false
                first_get_order?.isEnabled = true
                firstOrderComplete = false
            } else if (firstHoldStatus) {
                utils.updateOrderStatus(firstId, "4",this)
                first_order_complete?.isEnabled = false
                first_get_order?.isEnabled = true
                firstOrderComplete = false
                updateProduction(firstList)
            }
        }
    }

    private fun updateProduction(list: List<OrderIdProductItem?>) {
        list.forEach {
            try {
                Handler(Looper.getMainLooper()).postDelayed({
                    utils.insertWmsGoods(
                        it?.orderId,
                        InsertModel(
                            it?.sku,
                            it?.batchNo.toString(),
                            (-(it?.qty)!!).toString(),
                            it.bay,
                            "PASS"
                        )
                    )
                    mqtt.publish("location", "DataChange".toByteArray())
                }, 500)
            } catch (e: Exception) {

            }
        }

    }

    @SuppressLint("SetTextI18n")
    private fun searchClicks() {

        b8?.setOnClickListener {
            performClick("B-8")
        }
        b7?.setOnClickListener {
            performClick("B-7")
        }
        b6?.setOnClickListener {
            performClick("B-6")
        }
        b5?.setOnClickListener {
            performClick("B-5")
        }
        b4?.setOnClickListener {
            performClick("B-4")
        }
        b3?.setOnClickListener {
            performClick("B-3")
        }
        b2?.setOnClickListener {
            performClick("B-2")
        }
        b1?.setOnClickListener {
            performClick("B-1")
        }
        forklift?.setOnClickListener {
            performClick("FORKLIFT-PATH")
        }


        btl_fifo?.setOnClickListener {
            searchAdapter.sortFifo()

        }
        btl_lifo?.setOnClickListener {
            searchAdapter.sortLifo()
        }

        tts_on?.setOnClickListener {
            tts_off?.visibility = VISIBLE
            tts_on?.visibility = GONE
            try {
                if (tts1!!.isSpeaking) {
                    tts1!!.stop()
                    val fullSetVolume = 0
                    mAudioManager?.setStreamVolume(AudioManager.STREAM_MUSIC, fullSetVolume, 0)
                }
            } catch (e: java.lang.Exception) {
                Log.d("TAG", "onClick: $e")
            }
        }

        tts_off?.setOnClickListener {
            tts_off?.visibility = GONE
            tts_on?.visibility = VISIBLE
            try {
                val setVolume = 10
                mAudioManager?.setStreamVolume(AudioManager.STREAM_MUSIC, setVolume, 0)
            } catch (e: java.lang.Exception) {
                Log.d("TAG", "onClick: $e")
            }


        }

        et_search?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                keySearch = p0!!.toString()
                when (searchSpinnerItem) {
                    "Batch No" -> {
                        val list = Search().searchFromBatch(keySearch)
                        searchAdapter.setList(list)
                        tts(list)
                    }
                    "Bay No" -> {
                        val list = Search().searchFromBay(keySearch)
                        searchAdapter.setList(list)
                        tts(list)
                    }
                    "Select…" -> {
                        val list = Search().searchFromAll(keySearch)
                        searchAdapter.setList(list)
                        tts(list)
                    }
                }
            }
        })

        search_spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                searchSpinnerItem = search_spinner.selectedItem.toString()

                Log.d("TAG", "search_spinner$searchSpinnerItem")
                when (searchSpinnerItem) {
                    "Select…" -> {
                        et_search.isEnabled = true
                    }
                    "All" -> {
                        val list = Search().searchFromAllExceptEmpty("empty")
                        searchAdapter.setList(list)
                        tts(list)
                        tv_search.text = "All Items"
                        et_search.isEnabled = false
                    }
                    "SKU" -> {
                        et_search.isEnabled = false
                        skuDialog.show()
                    }
                    "Block" -> {
                        et_search.isEnabled = false
                        blockDialog.show()
                    }
                    "Bay No" -> {
                        et_search.isEnabled = true
                        searchSpinnerItem = search_spinner.selectedItem.toString()
                        tv_search.text = "From BAY"
                    }
                    "Batch No" -> {
                        et_search.isEnabled = true
                        searchSpinnerItem = search_spinner.selectedItem.toString()
                        tv_search.text = "From Batch"
                    }
                }
            }
        }
    }
    private fun performClick(name: String) {
        if (this::handleClick.isInitialized) {
            handleClick.clickBay(name)
        }
    }
    private fun dataResponse() {
        try {
            utils.addCallStatus(object : CallStatus {
                override fun commonList(commonData: List<CommonDataItem?>?) {
                    try {
                        if (commonData.isNullOrEmpty()) {
                            when (count) {
                                1 -> {
                                    first_order_progressbar?.visibility = GONE
                                    first_get_order?.isEnabled = true
                                    first_order_complete?.isEnabled = false
                                    firstList.clear()
                                    firstOrderListAdapter.setList(
                                        firstList,
                                        true
                                    )

                                    first_vehicle_num.text = ""
                                    first_total_qty.text = ""
                                    first_truck_bay_no.text = ""
                                    first_order_id.text = ""
                                }
                                2 -> {
                                    second_order_progressbar?.visibility = GONE
                                    second_get_order?.isEnabled = true
                                    second_order_complete?.isEnabled = false
                                    secondList.clear()
                                    secondOrderListAdapter.setList(
                                        secondList,
                                        true
                                    )
                                    second_vehicle_num.text = ""
                                    second_total_qty.text = ""
                                    second_truck_bay_no.text = ""
                                    second_order_id.text = ""
                                }
                            }
                            Toast.makeText(
                                this@MainActivity,
                                "Doesn't have any order",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            when {
                                firstOrderStatus -> {
                                    getFirstOrder(commonData)
                                }
                                SecondOrderStatus -> {
                                    getSecondOrder(commonData)
                                }
                            }
                        }
                    } catch (e: Exception) {
                        print(e)
                    }
                }

                override fun orderList(orderIdProduct: List<OrderIdProductItem?>?) {
                    try {
                        if (orderIdProduct.isNullOrEmpty()) {
                            when (count) {
                                1 -> {
                                    first_order_progressbar?.visibility = GONE
                                    first_get_order?.isEnabled = true
                                    first_order_complete?.isEnabled = false
                                    firstList.clear()
                                    firstOrderListAdapter.setList(
                                        firstList,
                                        true
                                    )

                                    /*first_vehicle_num.text = ""
                                    first_total_qty.text = ""
                                    first_truck_bay_no.text = ""
                                    first_order_id.text = ""*/
                                }
                                2 -> {
                                    second_order_progressbar?.visibility = GONE
                                    second_get_order?.isEnabled = true
                                    second_order_complete?.isEnabled = false
                                    secondList.clear()
                                    secondOrderListAdapter.setList(
                                        secondList,
                                        true
                                    )
                                    /* second_vehicle_num.text = ""
                                     second_total_qty.text = ""
                                     second_truck_bay_no.text = ""
                                     second_order_id.text = ""*/
                                }
                            }
                        } else {
                            when (count) {
                                1 -> {
                                    first_order_progressbar?.visibility = GONE
                                    first_get_order?.isEnabled = false
                                    first_order_complete?.isEnabled = true
                                    firstOrderListAdapter.setList(
                                        (orderIdProduct as ArrayList<OrderIdProductItem?>),
                                        firstHoldStatus
                                    )
                                    var status = 0
                                    orderIdProduct.forEach {
                                        if (it?.qty != 0) {
                                            status++
                                        }
                                    }
                                    if (status == 0) {
                                        firstOrderComplete = true
                                    }
                                    if (firstHoldStatus) firstList = orderIdProduct
                                }
                                2 -> {
                                    second_order_progressbar?.visibility = GONE
                                    second_get_order?.isEnabled = false
                                    second_order_complete?.isEnabled = true
                                    secondOrderListAdapter.setList(
                                        orderIdProduct as ArrayList<OrderIdProductItem?>,
                                        secondHoldStatus
                                    )
                                    var status = 0
                                    orderIdProduct.forEach {
                                        if (it?.qty != 0) {
                                            status++
                                        }
                                    }
                                    if (status == 0) {
                                        secondOrderComplete = true
                                    }

                                    if (secondHoldStatus)
                                        secondList = orderIdProduct
                                }
                            }
                        }
                    } catch (e: Exception) {
                    }
                }

                override fun changeOrder(locationChange: List<LocationChangeItem?>?) {
                    try {
                        if (locationChange.isNullOrEmpty()) {
                            when (count) {
                                1 -> {
                                    first_order_progressbar?.visibility = GONE
                                    first_get_order?.isEnabled = true
                                    first_order_complete?.isEnabled = false
                                    firstChangeOrderAdapter.setList(
                                        locationChange as ArrayList<LocationChangeItem?>,
                                        firstHoldStatus
                                    )
                                }
                                2 -> {
                                    second_order_progressbar?.visibility = GONE
                                    second_get_order?.isEnabled = true
                                    second_order_complete?.isEnabled = false
                                    secondChangeOrderAdapter.setList(
                                        locationChange as ArrayList<LocationChangeItem?>,
                                        secondHoldStatus
                                    )
                                }
                            }
                        } else {
                            when (count) {
                                1 -> {
                                    first_order_progressbar?.visibility = GONE
                                    first_get_order?.isEnabled = false
                                    first_order_complete?.isEnabled = true
                                    firstChangeOrderAdapter.setList(
                                        locationChange as ArrayList<LocationChangeItem?>,
                                        firstHoldStatus
                                    )
                                    firstOrderComplete = true
                                    if (!firstHoldStatus) {
                                        changeProcessDialog.show()
                                        changeProcessDialog.common_msg.text =
                                            resources.getString(R.string.change_loc_msg)
                                    } else {
                                        first_vehicle_num.text = getString(R.string.order_cancel)
                                        first_total_qty.text = getString(R.string.order_cancel)
                                        first_truck_bay_no.text = getString(R.string.order_cancel)
                                    }
                                }
                                2 -> {
                                    second_order_progressbar?.visibility = GONE
                                    second_get_order?.isEnabled = false
                                    second_order_complete?.isEnabled = true
                                    secondChangeOrderAdapter.setList(
                                        locationChange as ArrayList<LocationChangeItem?>,
                                        secondHoldStatus
                                    )
                                    secondOrderComplete = true
                                    if (!secondHoldStatus) {
                                        changeProcessDialog.show()
                                        changeProcessDialog.common_msg.text =
                                            resources.getString(R.string.change_loc_msg)
                                    } else {
                                        second_total_qty?.text = getString(R.string.order_cancel)
                                        second_total_qty?.text = getString(R.string.order_cancel)
                                        second_vehicle_num?.text = getString(R.string.order_cancel)
                                    }
                                }
                            }
                        }
                    } catch (e: Exception) {
                    }
                }

                override fun itemUpdated(msg: String?) {
                    try {
                        if (!msg.isNullOrBlank()) {
                            when (count) {
                                1 -> {
                                    setFirstOrder()
                                    orderProcessDialog.dismiss()
                                    orderProcessDialog.order_batch_no?.text = ""
                                    orderProcessDialog.order_bay_no?.text = ""
                                    orderProcessDialog.order_qty?.setText("")
                                    orderProcessDialog.order_sku?.text = ""
                                }
                                2 -> {
                                    setSecondOrder()
                                    orderProcessDialog.dismiss()
                                    orderProcessDialog.order_batch_no?.text = ""
                                    orderProcessDialog.order_bay_no?.text = ""
                                    orderProcessDialog.order_qty?.setText("")
                                    orderProcessDialog.order_sku?.text = ""
                                }
                            }
                        } else {
                            Toast.makeText(
                                this@MainActivity,
                                "Something went wrong!!!",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                    } catch (e: Exception) {
                    }
                }

                override fun transportList(transportDetailsList: List<TransportStatusDataItem?>?) {
                    try {
                        transportDetailsList?.forEach {
                            try {
                                if (it?.orderId.toString() == secondOrderId) {
                                    second_vehicle_num.text = it?.vehicleNo
                                    second_total_qty.text = it?.total_qty
                                    second_truck_bay_no.text = it?.truckBayNo
                                } else if (it?.orderId.toString() == firstOrderId) {
                                    first_vehicle_num.text = it?.vehicleNo
                                    first_total_qty.text = it?.total_qty
                                    first_truck_bay_no.text = it?.truckBayNo
                                }
                            } catch (e: Exception) {
                            }
                        }
                    } catch (e: Exception) {
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    private fun setFirstOrder() {
        try {
            first_order_progressbar?.visibility = VISIBLE
            count = 1
            firstId = ""
            firstOrderStatus = true
            SecondOrderStatus = false
            utils.getCommonList()
        } catch (e: Exception) {
        }
    }

    private fun setSecondOrder() {
        try {
            second_order_progressbar?.visibility = VISIBLE
            count = 2
            secondId = ""
            SecondOrderStatus = true
            firstOrderStatus = false
            utils.getCommonList()
        } catch (e: Exception) {
        }
    }

    private fun showOrderScreen() {
        try {
            wms_search.visibility = GONE
            wms_order.visibility = VISIBLE
            val am = getSystemService(Context.AUDIO_SERVICE) as AudioManager

            am.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                0,
                0
            )
        } catch (e: Exception) {
        }
    }

    private fun getFirstOrder(commonList: List<CommonDataItem?>?) {
        firstOrderId = handleSharedPreference.firstOrderNoPref()!!
        var i = 0
        var hasValue = false
        while (i < commonList!!.size && !hasValue) {
            val list = commonList[i]
            try {
                if (list?.orderId.toString() != secondOrderId) {
                    when (list?.status) {
                        1 -> {
                            try {
                                if (firstOrderId == list.orderId.toString()) {
                                    try {
                                        firstOrderStatus = if (list.type == "order") {
                                            setFirstAdapter()
                                            utils.getOrderDetail(list.orderId.toString())
                                            utils.getTransportDetails(list.orderId.toString())
                                            mqtt.publish(
                                                "running",
                                                list.orderId.toString().toByteArray()
                                            )
                                            false
                                        } else {
                                            setFirstChangeAdapter()
                                            utils.getLocationChangeData(list.orderId.toString())
                                            first_vehicle_num.text = getString(R.string.loc_change)
                                            first_total_qty.text = getString(R.string.loc_change)
                                            first_truck_bay_no.text = getString(R.string.loc_change)
                                            false
                                        }
                                    } catch (e: Exception) {
                                    }
                                    firstId = list.id!!.toString()
                                    first_order_id?.text = list.orderId.toString()
                                    hasValue = !hasValue
                                    firstHoldStatus = false
                                }
                            } catch (e: Exception) {
                            }
                        }
                        0 -> {
                            try {
                                try {
                                    firstOrderStatus = if (list.type == "order") {
                                        setFirstAdapter()
                                        utils.getOrderDetail(list.orderId.toString())
                                        utils.getTransportDetails(list.orderId.toString())
                                        mqtt.publish(
                                            "running",
                                            list.orderId.toString().toByteArray()
                                        )
                                        false
                                    } else {
                                        setFirstChangeAdapter()
                                        utils.getLocationChangeData(list.orderId.toString())
                                        first_vehicle_num.text = getString(R.string.loc_change)
                                        first_total_qty.text = getString(R.string.loc_change)
                                        first_truck_bay_no.text = getString(R.string.loc_change)
                                        false
                                    }
                                } catch (e: Exception) {
                                }
                                firstId = list.id!!.toString()
                                utils.updateOrderStatus(firstId, "1",this)
                                handleSharedPreference.insertFirstOrderNo(list.orderId.toString())
                                firstOrderId = list.orderId.toString()
                                first_order_id?.text = list.orderId.toString()
                                hasValue = !hasValue
                                firstHoldStatus = false
                            } catch (e: Exception) {
                            }
                        }
                        3 -> {
                            Log.d("TAG", "getFirstOrder: ${list.status}")
                            try {
                                try {
                                    firstHoldStatus = if (list.type == "order") {
                                        setFirstAdapter()
                                        utils.getOrderDetail(list.orderId.toString())
                                        utils.getTransportDetails(list.orderId.toString())
                                        firstOrderStatus = false
                                        true
                                    } else {
                                        setFirstChangeAdapter()
                                        utils.getLocationChangeData(list.orderId.toString())
                                        first_vehicle_num.text = getString(R.string.order_cancel)
                                        first_total_qty.text = getString(R.string.order_cancel)
                                        first_truck_bay_no.text = getString(R.string.order_cancel)
                                        firstOrderStatus = false
                                        true
                                    }
                                } catch (e: Exception) {
                                }
                                firstId = list.id!!.toString()
                                handleSharedPreference.insertFirstOrderNo(list.orderId.toString())
                                first_order_id?.text = list.orderId.toString()
                                changeProcessDialog.show()
                                changeProcessDialog.common_msg.text =
                                    resources.getString(R.string.order_is_cancelled)
                                hasValue = !hasValue
                            } catch (e: Exception) {
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }
            i++
        }
    }

    private fun getSecondOrder(commonData: List<CommonDataItem?>) {
        secondOrderId = handleSharedPreference.secondOrderNoPref()!!
        var i = 0
        var hasValue = false
        while (i < commonData.size && !hasValue) {
            val list = commonData[i]
            try {
                if (firstOrderId != list?.orderId.toString()) {
                    when (list?.status) {
                        1 -> {
                            try {
                                if (secondOrderId == list.orderId.toString()) {
                                    try {
                                        SecondOrderStatus = if (list.type == "order") {
                                            setSecondAdapter()
                                            utils.getOrderDetail(list.orderId.toString())
                                            utils.getTransportDetails(list.orderId.toString())
                                            mqtt.publish(
                                                "running",
                                                list.orderId.toString().toByteArray()
                                            )
                                            false
                                        } else {
                                            utils.getLocationChangeData(list.orderId.toString())
                                            setSecondChangeAdapter()
                                            second_total_qty?.text = getString(R.string.loc_change)
                                            second_total_qty?.text = getString(R.string.loc_change)
                                            second_vehicle_num?.text =
                                                getString(R.string.loc_change)
                                            false
                                        }
                                    } catch (e: Exception) {
                                    }
                                    secondId = list.id!!.toString()
                                    second_order_id?.text = list.orderId.toString()
                                    hasValue = !hasValue
                                    secondHoldStatus = false
                                }
                            } catch (e: Exception) {
                            }
                        }
                        0 -> {
                            try {
                                try {
                                    SecondOrderStatus = if (list.type == "order") {
                                        setSecondAdapter()
                                        utils.getOrderDetail(list.orderId.toString())
                                        utils.getTransportDetails(list.orderId.toString())
                                        mqtt.publish(
                                            "running",
                                            list.orderId.toString().toByteArray()
                                        )
                                        false
                                    } else {
                                        utils.getLocationChangeData(list.orderId.toString())
                                        setSecondChangeAdapter()
                                        second_total_qty?.text = getString(R.string.loc_change)
                                        second_total_qty?.text = getString(R.string.loc_change)
                                        second_vehicle_num?.text = getString(R.string.loc_change)
                                        false
                                    }
                                } catch (e: Exception) {
                                }
                                secondId = list.id!!.toString()
                                second_order_id?.text = list.orderId.toString()
                                handleSharedPreference.insertSecondOrderNo(list.orderId.toString())
                                secondOrderId = list.orderId.toString()
                                utils.updateOrderStatus(secondId, "1",this)
                                secondHoldStatus = false
                                hasValue = !hasValue
                            } catch (e: Exception) {
                            }

                        }
                        3 -> {
                            Log.d("TAG", "getSecondOrder: ${list.status}")
                            try {
                                try {
                                    secondHoldStatus = if (list.type == "order") {
                                        setSecondAdapter()
                                        utils.getOrderDetail(list.orderId.toString())
                                        utils.getTransportDetails(list.orderId.toString())
                                        SecondOrderStatus = false
                                        true
                                    } else {
                                        utils.getLocationChangeData(list.orderId.toString())
                                        setSecondChangeAdapter()
                                        second_total_qty?.text = getString(R.string.order_cancel)
                                        second_total_qty?.text = getString(R.string.order_cancel)
                                        second_vehicle_num?.text = getString(R.string.order_cancel)
                                        SecondOrderStatus = false
                                        true
                                    }
                                } catch (e: Exception) {
                                }
                                secondId = list.id!!.toString()
                                second_order_id?.text = list.orderId.toString()
                                handleSharedPreference.insertSecondOrderNo(list.orderId.toString())
                                hasValue = !hasValue
                                changeProcessDialog.show()
                                changeProcessDialog.common_msg.text =
                                    resources.getString(R.string.order_is_cancelled)
                            } catch (e: Exception) {
                            }
                        }
                    }
                }

            } catch (e: Exception) {
            }
            i++
        }
    }

    private fun submitSingleItem(
        currentOrderId: Int,
        bayNo: String,
        batchNo: String,
        sku: String,
        newProcessQty: Int
    ) {
        utils.insertOutGoods(
            InsertOutGoods(
                batchNo,
                bayNo,
                sku,
                newProcessQty.toString(),
                currentOrderId.toString()
            ), changeInQty
        )

        utils.insertWmsGoods(
            currentOrderId,
            InsertModel(sku, batchNo, changeInQty.toString(), bayNo, "PASS")
        )

        mqtt.publish("location", "DataChange".toByteArray())

    }

    companion object {
        var firstOrderComplete = false
        var secondOrderComplete = false
        var firstOrderStatus = false
        var SecondOrderStatus = false
        var firstHoldStatus = false
        var secondHoldStatus = false
        var firstId = ""
        var firstOrderId = ""
        var secondId = ""
        var secondOrderId = ""
        var count = 0
    }

    private fun setSearch() {
        try {
            wms_search.visibility = VISIBLE
            wms_order.visibility = GONE

            setSearchAdapter()
            val am = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            tts_off?.visibility = GONE
            tts_on?.visibility = VISIBLE
            am.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                0
            )
        } catch (e: Exception) {
        }
    }

    private fun setSearchAdapter() {
        try {
            searchAdapter = SearchAdapter.getInstance()
            val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            rv_search.layoutManager = linearLayoutManager
            rv_search.adapter = searchAdapter
        } catch (e: Exception) {
        }
    }

    private fun setFirstAdapter() {
        try {
            firstOrderListAdapter = FirstOrderListAdapter.getInstance()
            val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            first_order_recycler_view.layoutManager = linearLayoutManager
            first_order_recycler_view.adapter = firstOrderListAdapter
        } catch (e: Exception) {
        }

        firstOrderListAdapter.addItemClick(object : SetFirstItemClick {
            @SuppressLint("SetTextI18n")
            override fun onClick(
                bay: String?,
                sku: String?,
                batchNo: String?,
                qty: Int?,
                orderId: Int?
            ) {
                try {
                    count = 1
                    if (qty != 0) {
                        orderProcessDialog.show()
                        orderProcessDialog.order_bay_no.text = bay
                        orderProcessDialog.order_sku.text = sku
                        orderProcessDialog.order_batch_no.text = batchNo.toString()
                        orderProcessDialog.total_order_qty.text =
                            resources.getString(R.string.total_order_qty) + " $qty"
                        currentOrderId = orderId!!
                        oldProcessQty = qty!!
                    }
                } catch (e: Exception) {
                }
            }
        })
    }

    private fun setSecondAdapter() {
        try {
            secondOrderListAdapter = SecondOrderListAdapter.getInstance()
            val sLinearLayoutManager =
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            second_order_recycler_view.layoutManager = sLinearLayoutManager
            second_order_recycler_view.adapter = secondOrderListAdapter
        } catch (e: Exception) {
        }

        secondOrderListAdapter.addItemClick(object : SetSecondItemClick {
            @SuppressLint("SetTextI18n")
            override fun onClick(
                bay: String?,
                sku: String?,
                batchNo: String?,
                qty: Int?,
                orderId: Int?
            ) {
                try {
                    count = 2
                    if (qty != 0) {
                        orderProcessDialog.show()
                        orderProcessDialog.order_sku.text = sku
                        orderProcessDialog.order_bay_no.text = bay
                        orderProcessDialog.order_batch_no.text = batchNo.toString()
                        orderProcessDialog.total_order_qty.text =
                            resources.getString(R.string.total_order_qty) + " $qty"
                        currentOrderId = orderId!!
                        oldProcessQty = qty!!
                    }
                } catch (e: Exception) {
                }
            }
        })
    }

    private fun setFirstChangeAdapter() {
        try {
            firstChangeOrderAdapter = ChangeOrderAdapter()
            val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            first_order_recycler_view.layoutManager = linearLayoutManager
            first_order_recycler_view.adapter = firstChangeOrderAdapter
        } catch (e: Exception) {
        }

        firstChangeOrderAdapter.addItemClick(object : SetItemClick {
            override fun onclick(
                oldBay: String?,
                bayNo: String?,
                sku: String?,
                batchNo: String,
                qty: String
            ) {

            }
        })
    }

    private fun setSecondChangeAdapter() {
        try {
            secondChangeOrderAdapter = ChangeOrderAdapter()
            val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            second_order_recycler_view.layoutManager = linearLayoutManager
            second_order_recycler_view.adapter = secondChangeOrderAdapter
        } catch (e: Exception) {
        }
        secondChangeOrderAdapter.addItemClick(object : SetItemClick {
            override fun onclick(
                oldBay: String?,
                bayNo: String?,
                sku: String?,
                batchNo: String,
                qty: String
            ) {
            }
        })
    }

    override fun onInit(status: Int) {
        try {
            if (status == TextToSpeech.SUCCESS) {
                val result: Int = tts1?.setLanguage(Locale.US)!!
                if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED
                ) {
                    Log.d("textToSpeech", "This Language is not supported")
                } else {
                    Log.d("TAG", "onInit: success")
                }
            } else {
                Log.d("textToSpeech", "Initialization Failed!")
            }
        } catch (e: Exception) {
            Log.d("TAG", "onInit: $e")
        }
    }

    fun tts(list: ArrayList<ProductionItem>) {
        var speechText: String
        if (tts1!!.isSpeaking) {
            tts1!!.stop()
        }
        try {
            list.indices.forEach { i ->
                speechText = "bay " + java.lang.String.valueOf(list[i].bayNo) +
                        ",S.K.U" + java.lang.String.valueOf(list[i].sku) +
                        ",lot no." + java.lang.String.valueOf(list[i].batchNo) +
                        ",Quantity " + java.lang.String.valueOf(list[i].qty)
                tts1!!.speak(speechText, TextToSpeech.QUEUE_ADD, null, "")
            }
        } catch (e: java.lang.Exception) {
            Log.e("TAG", "tts: ", e)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (tts1?.isSpeaking!!) {
            tts1?.stop()
            tts1?.shutdown()
        }
    }
}