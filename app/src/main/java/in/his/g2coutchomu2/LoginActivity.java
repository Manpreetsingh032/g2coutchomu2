package in.his.g2coutchomu2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import in.his.g2coutchomu2.apiinterface.ApiService;
import in.his.g2coutchomu2.apiinterface.RetroClient;
import in.his.g2coutchomu2.dataModel.Password;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    public static Retrofit retrofit;
    public static ApiService apiservice;
    final String userId = "pernod";
    EditText password;
    TextView btn_login;
    Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_login);
        retrofit = RetroClient.getClient();
        apiservice = retrofit.create(ApiService.class);
        utils = new Utils();
        password = findViewById(R.id.login_et_password);
        btn_login = findViewById(R.id.login_tv_login);
        btn_login.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_tv_login) {
            checkPassword();
        }
    }

    /*{"userId":"wms","password":"12356"}*/

    private void checkPassword() {
        final String pass = password.getText().toString();
        if (!pass.isEmpty()) {
            Call<Password> passwordCall = apiservice.PASSWORD_CALL(userId, pass);
            passwordCall.enqueue(new Callback<Password>() {
                @Override
                public void onResponse(Call<Password> call, Response<Password> response) {
                    if (response.body() != null && response.body().getMessage() != null) {
                        if ("Successful".equals(response.body().getMessage())) {
                            utils.getSku(LoginActivity.this);
                            utils.getBay();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        } else {
                            password.setError("Wrong Password");
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Please Check Network Connection First", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Password> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.getMessage());
                    Toast.makeText(LoginActivity.this, "Please Check Network Connection First", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
