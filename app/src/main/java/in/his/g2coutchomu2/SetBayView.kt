package `in`.his.g2coutchomu2

interface SetBayView {
    fun changeView()
}

class ViewListener {
    private var setBayView = ArrayList<SetBayView>()
    fun addBayView(setBayView: SetBayView) {
        this.setBayView.add(setBayView)
    }

    fun changeView() {
        setBayView.forEach { it.changeView() }
    }

    companion object {
        private lateinit var viewListener: ViewListener
        fun instance(): ViewListener {
            return if (this::viewListener.isInitialized) {
                viewListener
            } else {
                viewListener = ViewListener()
                viewListener
            }
        }
    }
}